#include <cs50.h>
#include <stdio.h>
#include <math.h>

#define QUARTER 25;
#define DIME 10;
#define NICKEL 5;

int main(void)
{
    // Variables
    float n = 0;
    int cent_ammount = 0;
    int quarter_count = 0;
    int dime_count = 0;
    int nickel_count = 0;
    int coin_count = 0;
    int leftover = 0;

    // Get ammount in dollars.
    do
    {
        printf("How much is owed? $ ");
        n = GetFloat();
    }
    while (n <= 0);
        // if quarters can be used, increase count, check dimes,
        // check if pennies can be used, increase count
        // Return count.(number of coints used.)
        
        // Converts Dollar into cents.
    cent_ammount = (int) round(n * 100);
        
        // Check's how many quarters.
    quarter_count = cent_ammount / QUARTER;
    leftover = cent_ammount % QUARTER;
        
        // Checks how many dimes.
    dime_count = leftover / DIME;
    leftover = leftover % DIME;

        // Check's how many nickels.
    nickel_count = leftover / NICKEL;
    leftover = leftover % NICKEL;

    coin_count = quarter_count + dime_count + nickel_count + leftover;

    printf("%d\n", coin_count);

    return 0;
}

