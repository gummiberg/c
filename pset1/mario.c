#include <cs50.h>
#include <stdio.h>

int main(void)
{
    // Declare Variable.
    int n;

    // Grab User input. Check and ask for retry if input = 0
    do
    {
        printf("rows of pyramid? (0-23).\n");
        n = GetInt();
        // if input is 0 program stops.
        if (n == 0)
        {
            return 0;
        }
    }
    while (n < 1 || n > 23);
    // for loop, i iterates over rows, j adds spaces and k adds #
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n - i - 1; j++) 
        {
            printf(" ");
        }
        for (int k = 0; k < i + 2; k++)
        {
            printf("#");
        }
        printf("\n");    
    }

}
